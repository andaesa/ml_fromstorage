import os
import sys

from flask import Flask

PID_FILE = None

monitor_app = Flask(__name__)

@monitor_app.route('/_ah/health')
def health():
	if not os.path.exists(PID_FILE):
		return 'Worker pid not found', 503

	with open(PID_FILE, 'r') as pidfile:
		pid = pidfile.read()

	if not os.path.exists('/proc/{}'.format(pid)):
		return 'Worker not running', 503

	return 'healthy', 200

@monitor_app.route('/')
def index():
	return health()

if __name__ == '__main__':
	PID_FILE = sys.argv[1]
	monitor_app.run('0.0.0.0', 8080)