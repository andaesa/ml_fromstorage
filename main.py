import machinelearning
import config

app = machinelearning.create_app(config)

with app.app_context():
	ml_train = machinelearning.tasks.get_ml_train()

if __name__ == '__main__':
	app.run(host='127.0.0.1', port=8080, debug=True)