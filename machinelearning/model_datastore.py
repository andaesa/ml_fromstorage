from flask import current_app
from google.cloud import datastore


builtin_list = list

def init_app(app):
	pass

def get_client():
	return datastore.Client(current_app.config['PROJECT_ID'])

def from_datastore(entity):
	if not entity:
		return None

	if isinstance(entity, builtin_list):
		entity = entity.pop()

	entity['id'] = entity.key.id
	return entity

def update(data, id=None):
	ds = get_client()
	if id:
		key = ds.key('The_File', int(id))
	else:
		key = ds.key('The_File')

	entity = datastore.Entity(
		key=key,
		exclude_from_indexes=['description'])

	entity.update(data)
	ds.put(entity)
	return from_datastore(entity)

create = update
