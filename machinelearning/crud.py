from machinelearning import get_model, oauth2, storage, tasks
from flask import Blueprint, current_app, redirect, render_template, request, \
    session, url_for

crud = Blueprint('crud', __name__)

def upload_csv_file(file):
	if not file:
		return None

	public_url = storage.upload_file(
		file.read(),
		file.filename,
		file.content_type
	)

	current_app.logger.info(
		"Uploaded file %s as %s.", file.filename, public_url)

	return public_url



@crud.route("/")
def list():
	token = request.args.get('page_token', None)
	if token:
		token = token.encode('utf-8')

	files, next_page_token = get_model().list(cursor=token)

	return 'Test1'


@crud.route("/mine")
@oauth2.required
def list_mine():
	token = request.args.get('page_token', None)
	if token:
		token = token.encode('utf-8')

	files, next_page_token = get_model().list_by_user(
        user_id=session['profile']['id'],
        cursor=token)

	"""return render_template(
        "list.html",
        books=books,
        next_page_token=next_page_token)"""
	return 'Test2'


@crud.route('/<id>')
def view(id):
    file = get_model().read(id)
    #return render_template("view.html", book=book)
    return 'Test3'


@crud.route('/add', methods=['GET', 'POST'])  #### kena update sikit pasal kat sini ###
def add():
	if request.method == 'POST':
		data = request.form.to_dict(flat=True)

        # If a file was uploaded, update the data to point to the new file.
		csv_url = upload_csv_file(request.files.get('file'))

		if csv_url:
			data['csvUrl'] = csv_url

        # If the user is logged in, associate their profile with the new book.  ### kena update part ni ###
		"""if 'profile' in session:
			data['createdBy'] = session['profile']['displayName']
			data['createdById'] = session['profile']['id']"""

		file = get_model().create(data)

        # [START enqueue]
		q = tasks.get_ml_train()
		q.enqueue(tasks.train_file, file['id'])
        # [END enqueue]

        #return redirect(url_for('.view', id=book['id']))
		return 'is processing...'

    #return render_template("form.html", action="Add", book={})
	return 'still processing...'


"""@crud.route('/<id>/edit', methods=['GET', 'POST'])
def edit(id):
	book = get_model().read(id)

	if request.method == 'POST':
		data = request.form.to_dict(flat=True)

		image_url = upload_image_file(request.files.get('image'))

		if image_url:
			data['imageUrl'] = image_url

		book = get_model().update(data, id)

		q = tasks.get_books_queue()
		q.enqueue(tasks.process_book, book['id'])

		return redirect(url_for('.view', id=book['id']))

	return render_template("form.html", action="Edit", book=book)"""


"""@crud.route('/<id>/delete')
def delete(id):
	get_model().delete(id)
	return redirect(url_for('.list'))"""