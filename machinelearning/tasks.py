from flask import current_app
from google.cloud import pubsub
from machinelearning import get_model, storage
from keras.models import Sequential
from keras.layers import Dense

import numpy
import psq
import requests
import json

## Task to train the file using Keras ##

def get_ml_train():
	client = pubsub.Client(
		project=current_app.config['PROJECT_ID'])

	return psq.Queue(
		client, 'files', extra_context=current_app.app_context)

def train_file(file):
	#return 'four o five'
	#_model = get_model()
	#the_file = _model.read(file)
	file = file.csvUrl

	dataset = numpy.loadtxt(file, delimiter=",")

	X = dataset[:,0:len(dataset)]
	Y = dataset[:,len(dataset)]

	# create model
	model = Sequential()
	model.add(Dense(12, input_dim=8, init='uniform', activation='relu'))
	model.add(Dense(8, init='uniform', activation='relu'))
	model.add(Dense(1, init='uniform', activation='sigmoid'))

	# Compile model
	model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])

	# Fit the model
	model.fit(X, Y, nb_epoch=1000, batch_size=10)

	# evaluate the model
	scores = model.evaluate(X, Y)
	return scores
	#print("%s: %.2f%%" % (model.metrics_names[1], scores[1]*100))

	"""model_json = model.to_json()
	with open("indian.json", "w") as json_file:
    	json_file.write(model_json)

	model.save_weights('indian.h5')"""





