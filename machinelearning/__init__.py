import json
import logging

from flask import current_app, Flask, redirect, request, session, url_for
from google.cloud import error_reporting
import google.cloud.logging
import httplib2
from oauth2client.contrib.flask_util import UserOAuth2

oauth2 = UserOAuth2()

def create_app(config, debug=False, testing=False, config_overrides=None):
	app = Flask(__name__)
	app.config.from_object(config)

	app.debug = debug
	app.testing = testing

	if config_overrides:
		app.config.update(config_overrides)


	if not app.testing:
		client = google.cloud.logging.Client(app.config['PROJECT_ID'])
		client.setup_logging(logging.INFO)

	with app.app_context():
		model = get_model()
		model.init_app(app)

	oauth2.init_app(
		app,
		scopes=['email', 'profile'],
		authorize_callback=_request_user_info)

	"""@app.route('/logout')
	def logout():
		del session['profile']
		session.modified = True
		oauth2.storage.delete()
		return redirect(request.referrer or '/')"""

	from .crud import crud
	app.register_blueprint(crud, url_prefix='/ml')

	@app.route("/")
	def index():
		return redirect(url_for('crud.add'))

	@app.errorhandler(500)
	def server_error(e):
		client = error_reporting.Client(app.config['PROJECT_ID'])
		client.report_exception(
			http_context=error_reporting.build_flask_context(request))
		return """
		An internal error occurred.
		""", 500

	return app


def get_model():
	model_backend = current_app.config['DATA_BACKEND']
	if model_backend == 'cloudsql':
		from . import model_cloudsql
		model = model_cloudsql
	elif model_backend == 'datastore':
		from . import model_datastore
		model = model_datastore
	elif model_backend == 'mongodb':
		from . import model_mongodb
		model = model_mongodb
	else:
		raise ValueError(
			"No appropriate databackend configured. "
			"Please specify datastore, cloudsql, or mongodb")

	return model


def _request_user_info(credentials):
	http = httplib2.Http()
	credentials.authorize(http)
	resp, content = http.request(
		'https://www.googleapis.com/plus/v1/people/me')

	if resp.status != 200:
		current_app.logger.error(
			"Error while obtaining user profile: %s" % resp)
		return None

	session['profile'] = json.loads(content.decode('utf-8'))